The project provides a basic structure for apps that are using tab bar navigation with scrollable bottom sheet. For more see the demo direcotry. 

If you are interested in working with [b]Vattenfall[b], please contact me at artur.gurgul@vattenfall.com

![List view](/demo/list.gif "List View")
